const express = require("express")
const routes = require('./routes')

const port = 3000

const servidor = express()

routes(servidor)

servidor.listen(port, () => {
    console.log(`Rodando na porta: ${port}`)
})
module.exports = servidor
const { Router } = require('espress')
const router = Router()
const DeuMatchController = require('../controllers/deuMatchController')

router.get('/deumatch/:nomeA/:nomeB', DeuMatchController.verificarMatch)
router.post('/deumatch', DeuMatchController.verificarMatch)


module.exports = router
import { HttpClient } from '@angular/common/http';import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Match } from '../models/Match';

@Injectable({
  providedIn: 'root'
})
export class DeuMatchService {
  private readonly URl = "https://3000-jonatasrmj-deumatch-uk7xcb5awxl.ws-us88.gitpod.io"
  constructor(
    private http: HttpClient
  ) {}

  buscarMatch(): Observable<any>{
    return this.http.get<any>(`${this.URl}/aleatorio`)
  }
  buscarMatchPost(match: Match ): Observable<any>{
    return this.http.post<any>(`${this.URl}/aleatorio`, match)
  }
}

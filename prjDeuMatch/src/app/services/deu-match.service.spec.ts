import { TestBed } from '@angular/core/testing';

import { DeuMatchService } from './deu-match.service';

describe('DeuMatchService', () => {
  let service: DeuMatchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeuMatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

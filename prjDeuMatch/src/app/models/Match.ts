export class Match{
    private _nome1: string;
    private _nome2: string;

    constructor(){
        this._nome1 = "";
        this._nome2 = "";
    }

    public set nome1 (nome1: string){
        this._nome1 = nome1
    }
    public get nome1(): string{
        return this._nome1;
    }
    public set nome2 (nome2: string){
        this._nome2 = nome2
    }
    public get nome2(): string{
        return this._nome2;
    }
}